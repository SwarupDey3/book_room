package com.mindtree.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.mindtree.entity.Room;
import com.mindtree.exception.NotFoundException;
import com.mindtree.service.MyStayService;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {
	
	@Autowired
	MyStayService service;

	
	//admin can add Room
	@PostMapping("/saveroom")
	public ResponseEntity<String> saveRoom(@RequestBody Room room) {
			
		String msg="Failed to Add";
		
		try {
				service.saveRoom(room);
				return new ResponseEntity<String>("Added",HttpStatus.FOUND);
			} catch (NotFoundException e) {
				
				
				return new ResponseEntity<String>(msg,HttpStatus.NOT_FOUND);
			}	
		
	}
		
	//admin can delete room
	@GetMapping("/deleteroom/{roomNo}")
	public ResponseEntity<String> deleteRoom(@PathVariable int roomNo) {
		System.out.println("GET"+roomNo);
		String msg="Failed to Delete";
		try {
				service.deleteRoom(roomNo);
				return new ResponseEntity<String>(msg,HttpStatus.FOUND);
			} catch (NotFoundException e) {
				
				
				return new ResponseEntity<String>(msg,HttpStatus.NOT_FOUND);
			}	
		
	}
	
	@GetMapping("/updateroom/{price}/{roomNo}")
	public ResponseEntity<String> updateRoomPrice(@PathVariable int roomNo,@PathVariable double price) {
	
		String msg="Failed to Update";
		try {
			
			service.updateRoomPrice(price, roomNo);
			return new ResponseEntity<String>(msg,HttpStatus.FOUND);
			} catch (NotFoundException e) {
				return new ResponseEntity<String>(msg,HttpStatus.NOT_FOUND);
			}	
			
	}
	
	@GetMapping("/getallrooms")
	public List<Room> getAllRooms(){
	try {
		return service.getAllRooms();
	}catch(NotFoundException e) {
		throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No Rooms Found");
	}
  }
}
