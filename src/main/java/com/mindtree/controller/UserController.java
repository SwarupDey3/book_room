package com.mindtree.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.mindtree.entity.Booking;
import com.mindtree.entity.Room;
import com.mindtree.entity.User;
import com.mindtree.exception.NotFoundException;
import com.mindtree.service.MyStayService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	@Autowired
	MyStayService service;

	@PostMapping("/saveuser")
	public ResponseEntity<String> saveUser(@RequestBody User user) {

		String msg = "Failed to Add";
		try {
			service.saveUser(user);
			return new ResponseEntity<String>(msg,HttpStatus.FOUND);
		} catch (NotFoundException e) {

			return new ResponseEntity<String>(msg,HttpStatus.NOT_FOUND);
		}
		
	}

	@GetMapping("/getuser/{userName}")
	public User getUser(@PathVariable String userName) {

		try {
			User user = service.getUserByName(userName);
			return user;
		} catch (NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
		}

	}

	@PostMapping("/savebooking")
	public ResponseEntity<String> saveBooking(@RequestBody Booking booking){
		String msg = "Failed to Add";
		try {
			service.saveBooking(booking);
			return new ResponseEntity<String>(msg,HttpStatus.FOUND);
		} catch (NotFoundException e) {
           return new ResponseEntity<String>(msg,HttpStatus.NOT_FOUND);
		}
		
	}

	@GetMapping("/getbooking")
	public List<Booking> getBooking() {
		return service.getBooking();
	}

	@GetMapping("/getrooms/{roomType}")
	public List<Room> getRoomByType(@PathVariable String roomType) {
		List<Room> roomList;
		try {
			roomList = service.getRoomByType(roomType);
			return roomList;
		} catch (NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Rooms Not Found");
		}

	}
	
	@GetMapping("/getoneroom/{roomNo}")
	public Room getOneRoom(@PathVariable int roomNo) {
		
		try {
			Room room=service.getOneRoom(roomNo);
			return room;
		} catch (NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Room Number Not Found");
		}
		
	}

}
