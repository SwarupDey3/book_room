package com.mindtree.exception;

@SuppressWarnings("serial")
public class NotFoundException extends Exception {

	public NotFoundException() {
		super();
	}
}
