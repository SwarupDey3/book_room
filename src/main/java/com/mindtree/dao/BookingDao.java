package com.mindtree.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mindtree.entity.Booking;

@Repository
public interface BookingDao  extends JpaRepository<Booking, Integer> {

}
