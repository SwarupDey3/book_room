package com.mindtree.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindtree.entity.Room;

@Repository
public interface RoomDao extends JpaRepository<Room, Integer>{

//	@Query("from Room r where r.roomType=?1")
//	public List<Room> getByType(String roomType);
	
	@Query(value="update Room r set r.roomPrice=?1 where r.roomNo=?2" )
	public void updateRoomPrice(double price,int roomNo);
	
	@Query("from Room r where r.roomNo=?1")
	public Room getOneRoom(int rooNo);
}
