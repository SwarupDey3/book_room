package com.mindtree.service.serviceImplementation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.mindtree.dao.BookingDao;
import com.mindtree.dao.RoomDao;
import com.mindtree.dao.UserDao;
import com.mindtree.entity.Booking;
import com.mindtree.entity.Room;
import com.mindtree.entity.User;
import com.mindtree.exception.NotFoundException;
import com.mindtree.service.MyStayService;

import ch.qos.logback.core.net.SyslogOutputStream;

@Service
public class MyStayServiceImplementation implements MyStayService {

	@Autowired
	BookingDao bookingRepo;

	@Autowired
	RoomDao roomRepo;

	@Autowired
	UserDao userRepo;

	@Override
	public void saveUser(User user) throws NotFoundException {
		try {
			userRepo.save(user);
		} catch (HibernateException e) {
			throw new NotFoundException();
		}
	}

	@Override
	public void saveRoom(Room room) throws NotFoundException {
		String msg = "Failed to Add!!!";
		try {
			roomRepo.save(room);
		} catch (HibernateException e) {
			throw new NotFoundException();
		}

	}

	@Override
	public List<Room> getRoomByType(String roomType) throws NotFoundException {

		try {
			List<Room> roomList = roomRepo.findAll();
			List<Room> roomListType = new ArrayList<>();
			roomList.stream().filter(room -> room.getRoomType().equalsIgnoreCase(roomType))
					.forEach(r -> roomListType.add(r));
			
			
			roomList.sort((Room a,Room b)-> a.getRoomNo()-b.getRoomNo());
			return roomListType;
			
			
		} catch (HibernateException e) {
			throw new NotFoundException();
		}
	}

	@Override
	public User getUserByName(String userName) throws NotFoundException {
		try {
			List<User> userList = userRepo.findAll();
			User user = new User();
			userList.stream().filter(usr -> usr.getUserName().equalsIgnoreCase(userName)).forEach(u -> {
				user.setUserId(u.getUserId());
				user.setPassword(u.getPassword());
				user.setUserName(u.getUserName());
				user.setAddress(u.getAddress());
				user.setGender(u.getGender());
				user.setUserRole(u.getUserRole());
			});

			return user;
		} catch (HibernateException e) {
			throw new NotFoundException();
		}
	}

	@Override
	public void deleteRoom(int roomNo) throws NotFoundException {
		// String msg="Failed to Add";
		try {
			Room room = roomRepo.getOne(roomNo);
			roomRepo.delete(room);
		} catch (HibernateException e) {
			throw new NotFoundException();
		}
	}

	@Override
	public void saveBooking(Booking booking) throws NotFoundException {
		System.out.println(booking);
		try {
			bookingRepo.save(booking);
		} catch (HibernateException e) {
			throw new NotFoundException();
		}

	}

	@Override
	public List<Booking> getBooking() {

		List<Booking> bookingList = bookingRepo.findAll();
		return bookingList;

	}

	@Override
	public List<Room> getAllRooms() throws NotFoundException {
		try{List<Room> roomList = roomRepo.findAll();
		
		
		roomList.sort((Room a,Room b)->a.getRoomNo()-b.getRoomNo());
		return roomList;
		}catch(HibernateException e) {
			throw new NotFoundException();
		}
		
	}


	@Override
	public void updateRoomPrice(double price, int roomNo) throws NotFoundException {
		try {

			roomRepo.updateRoomPrice(price, roomNo);

		} catch (HibernateException e) {
			throw new NotFoundException();
		}
		
	}
	
	public Room getOneRoom(int roomNo) throws NotFoundException {
		try {
			Room room=roomRepo.getOneRoom(roomNo);
			System.out.println(room.getRoomNo()+""+room.getRoomType());
			return room;
		}catch(HibernateException e) {
			throw new NotFoundException();
		}
		
	}
	
	public boolean checkRooms(int roomNo) {
		
	 Room room=roomRepo.getOneRoom(roomNo);
	 if(room==null)
		 return false;
	 else
		 return true;
		
	}

	
}
