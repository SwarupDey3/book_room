package com.mindtree.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mindtree.entity.Booking;
import com.mindtree.entity.Room;
import com.mindtree.entity.User;
import com.mindtree.exception.NotFoundException;

@Service
public interface MyStayService {

	public void saveUser(User user) throws NotFoundException;
	public void saveRoom(Room room) throws NotFoundException;
	public List<Room> getRoomByType(String roomType) throws NotFoundException;
	public User getUserByName(String userName) throws NotFoundException;
	public void deleteRoom(int roomNo) throws NotFoundException;
	public void saveBooking(Booking booking) throws NotFoundException;
	public List<Booking> getBooking();
	public List<Room> getAllRooms() throws NotFoundException;
	public void updateRoomPrice(double price, int roomNo) throws NotFoundException;
	public Room getOneRoom(int roomNo) throws NotFoundException;
}
