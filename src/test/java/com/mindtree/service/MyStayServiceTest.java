package com.mindtree.service;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mindtree.dao.UserDao;
import com.mindtree.entity.User;
import com.mindtree.exception.NotFoundException;
import com.mindtree.service.serviceImplementation.MyStayServiceImplementation;

@RunWith(MockitoJUnitRunner.class)
public class MyStayServiceTest {

	
	
	@InjectMocks
	MyStayServiceImplementation service;
	
	@Mock
	UserDao userDao;
	
	
	@Test
    public void getUserByNameTest() throws NotFoundException {
		
	   User user=new User();
	   user.setAddress("");
	   user.setGender("");
	   user.setUserId(1);
	   user.setUserRole("");
	   user.setUserName("Swarup");
	   
	   List<User> userList=new ArrayList<>();
	   
	   when(userDao.findAll()).thenReturn(userList);
	   
	   assertEquals(user, service.getUserByName("Swarup"));
	}

}
